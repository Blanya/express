var passwordValidator = require('password-validator');

// Create a schema
var schema = new passwordValidator();

// Add properties to it
schema
.is().min(8)                                    // Minimum length 8
.has().uppercase()                              // Must have uppercase letters
.has().lowercase()                              // Must have lowercase letters
.has().digits()                                // Must have at least 2 digits

exports.info = (message, data) =>
{
    return {
        message: message,
        data: data
    }
}

exports.passwordInstruction = (password) =>
{
    let pw;
    schema.validate(password) ? pw = password : pw = null;
    return pw;
}

exports.checkUsername = (username, usersTable) =>
{
    return usersTable.find(x => x.username == username);
}

exports.checkPassword = (username, password, usersTable) =>
{
    let user = usersTable.find(x => x.username == username);
    user.password == password ? user= true : user = false; 
    return user;
}