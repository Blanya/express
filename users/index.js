const express = require('express');
const app = express();
const fs = require('fs');
const bodyparser = require('body-parser');
const { info, passwordInstruction, checkUsername, checkPassword } = require('./helpers');

app.use(bodyparser.json());


app.listen(3000, console.log("Serveur connecté"));

const saveUsers = (user) =>
{
    fs.writeFileSync('users.json', JSON.stringify(user));
}

const getUsers = () =>
{
    usersContent = fs.readFileSync('users.json');
    users = JSON.parse(usersContent);
    return users;
}

app.post('/api/', (req, res)=>
{
    let message = "L'utilisateur a bien été créé";
    const user = req.body;

    if(checkUsername(user.username, getUsers()) == undefined)
    {
        const users = [...getUsers(), user];
        verif = passwordInstruction(user.password);
        verif!= null? (saveUsers(users), res.json(info(message, user))) : (message = "Votre mdp n'a pas le format requis", res.json(info(message, "1 majuscule, 1 minuscule, un chiffre, 8 caractères")));    
    }
    
    else
    {
        message = "Un utilisateur avec ce nom existe déjà";
        res.json(info(message, user.username));
    }
})

app.get("/api/:username", (req, res) =>
{
    const user = req.body; 

    if(checkUsername(user.username, getUsers()) == undefined)
    {
        message = "L'utilisateur entré n'existe pas";
        res.status(401).json(info(message, user.username));
    }
    else if(checkPassword(user.username, user.password, getUsers()) == false)
    {
        message = "Le mot de passe n'est pas le bon";
        res.status(401).json(info(message, "Veuillez réessayer " + user.username))
    }
    else 
    {
        message = `Bienvenue ${user.username}`;
        res.status(200).json(info(message, ""));
    }
});
