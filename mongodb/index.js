const express = require('express');
const app = express();
const port = 9000;
const mongoose = require('mongoose');
const studentsRoute = require('./routes/routes');
const url = "mongodb://127.0.0.1:27017/students"; 

//bodyparser auto
app
    .use(express.json())
    .use('/api/students', studentsRoute);

mongoose.connect(url, {useNewUrlParser: true});

const connex = mongoose.connection;

try{
    connex.on("open", () => {
        console.log("connected");
    });
}catch(error){
    console.log("Error" + error);
}

app.listen(port, () => {console.log("Le serveur est connecté")});