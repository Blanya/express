const express = require('express');
const Student = require('../models/studentModel.js');

const getStudents = async (req, res) => {
    try{
        const student = await Student.find();
        res.status(200).json(student);
    }catch (error){
        res.status(404).json({message: error.message});
    }
}

const getStudentByIdent = async (req, res) => {
    const identifiant = req.params.identifiant;
    try{
        const stud = await Student.findOne({
        identifiant : identifiant
        });
        res.status(200).json(stud);
    }catch(error){
        res.status(500).json({message:"Aucune donnée trouvé", data: error})
    }
}

const updateStudent = async (req, res) => {
    const identifiant = req.params.identifiant;
    const {name, registration, subjects} = req.body;
    try{
        const stud = await Student.findOneAndUpdate({
        identifiant : identifiant},
        {
            name: name? name : stud.name,
            registration: registration? registration : stud.registration,
            subjects: subjects? subjects : stud.subjects,
        })
        res.status(200).json({message: `L'étudiant avec l'identifiant ${identifiant} a bien été modifié`});
    }catch(error){
        res.status(500).json({message:"Aucune donnée trouvée", data: error.message})
    }
}

const deleteStudent = async (req, res) => {
    const identifiant = req.params.identifiant;
    try{
        const deleteStud = await Student.findOneAndDelete({
            identifiant: identifiant
        });
        res.status(200).json({message: "L'étudiant a bien été supprimé", data: deleteStud})
    }catch(error)
    {
        res.status(500).json({message: error.message})
    }
}

const newStudent = async (req, res) => {
    const newStudent = new Student({
        name : req.body.name, 
        identifiant : req.body.identifiant,
        registration : req.body.registration,
        subjects : req.body.subjects,
        registered_on: req.body.registered_on
    });
    try{
        await newStudent.save();
        res.status(201).json({message: "L'étudiant a bien été créé", data: newStudent});
    }catch(error){
        res.status(400).json({message: error.message});
    }
}

const upStud = async (req, res) => {
    const identifiant = req.params.identifiant;
    try{
        await Student.findOneAndUpdate({identifiant: identifiant}, {
            name: req.body.name,
            registration: req.body.registration,
            subjects : req.body.subjects, 
            registered_on : req.body.registered_on
        });
        res.status(202).json({identifiant: identifiant})
    }catch(error)
    {
        res.status(400).json({message: error.message});
    }
}

module.exports.getStudents = getStudents;
module.exports.getStudentByIdent = getStudentByIdent;
module.exports.deleteStudent = deleteStudent;
module.exports.updateStudent = updateStudent;
module.exports.newStudent = newStudent;