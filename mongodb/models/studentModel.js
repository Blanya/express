const mongoose = require('mongoose');

const studentSchema = mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    identifiant : {
        type: String,
        required: true,
        unique: true
    },
    registration : {
        type: String,
        required: true
    },
    subjects : {
        type: [String],
        required: true
    },
    registered_on : {
        type: Date,
        default: new Date()
    },
})

let studentData = mongoose.model('studentData', studentSchema);

module.exports = studentData;