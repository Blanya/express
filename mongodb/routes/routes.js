const express = require('express');
const student_action = require('../controller/studentController');

const router = express.Router();

router.get('/', student_action.getStudents);
router.post('/', student_action.newStudent);
router.get('/:identifiant', student_action.getStudentByIdent);
router.delete('/:identifiant', student_action.deleteStudent);
router.put('/:identifiant', student_action.updateStudent);

module.exports = router;