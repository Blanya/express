const express = require('express');
const app = express();
const bodyparser = require('body-parser');
const port = 3001;
const log = require('morgan');
const itemRoute = require('./routes/routes');

app
    .use(bodyparser.json())
    .use("/items", itemRoute);
    
app.listen(port, console.log("Serveur connecté .."))