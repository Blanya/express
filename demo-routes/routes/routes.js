const express = require('express');
const { getItems, addItem, getItemById, deleteItem, updateItem } = require('../controller/controllerItems');

const router = express.Router();

router.get('/', getItems);
router.post('/', addItem);
router.get('/:id', getItemById);
router.delete('/:id', deleteItem);
router.put('/:id', updateItem);

module.exports = router;