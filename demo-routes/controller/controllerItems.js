let itemsData = require("../db/items.json");
const {v4} = require("uuid")

const getItems = (req, res) => {
    res.json(itemsData);
}

const addItem = (req, res) => {
    const item = req.body;
    itemsData.push({...item, id:v4() });
    res.send(`Item ${item.name} a été créé avec succès`);
};

const getItemById = (req, res) => {
    const id = req.params.id;
    const itemFound = itemsData.find(x => x.id == id);
    res.json(`L'item ${itemFound.name} a été récupéré`);
}

const deleteItem = (req, res) => {
    const id = req.params.id;
    itemsData = itemsData.filter(x => x.id !== id);
    res.json(`L'item avec l'id ${id} a été supprimé`);
}

const updateItem = (req, res) =>  {
    const id = req.params.id;
    const {name, type, prix} = req.body;
    
    let itemUpdated = itemsData.find(x => x.id == id);

    if(name) itemUpdated.name = name;
    if(type) itemUpdated.type = type;
    if(prix) itemUpdated.prix = prix;

    res.json(`Item avec l'id ${id} et le nom ${name} a bien été modifié`);
}


module.exports = {getItems, getItemById, deleteItem, updateItem, addItem};