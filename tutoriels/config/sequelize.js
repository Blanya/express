const {Sequelize, DataTypes} = require('sequelize');
const tutorielModel = require('../models/tutoModel');
const tutos = require('../data/tutoriels.json');

const sequelize = new Sequelize('db_tuto', 'root', '', {
    host: 'localhost',
    dialect: 'mariadb',
    logging: false
});

const tuto = tutorielModel(sequelize, DataTypes);

const initDb = () => {
    return sequelize.sync({force: true}).then(_=> {
        tutos.map( tutoriel => {
            tuto.create({
            title: tutoriel.titre,
            description: tutoriel.description,
            published: tutoriel.published
        }).then(tutoriel => console.log(tutoriel.toJSON()))
    });
    console.log("La BDD a été initialisée");
    })
}

module.exports = {tuto, initDb}