const express = require('express');
const app = express();
const port = 3001;
const bodyparser = require('body-parser');
const tutoRoutes = require('./routes/routes');
const sequelize =  require('./config/sequelize');

app
    .use(bodyparser.json())
    .use('/api/tutoriels', tutoRoutes)

//initDB()
//sequelize.initDb();
app.listen(port, () => {console.log( `Serveur connecté, port ${port}`)});