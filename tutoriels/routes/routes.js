const express = require('express');
const { createTuto, getAllTutos, getOneTuto, updateTuto, deleteTuto, findPublished } = require('../controller/controllerTuto');

const router = express.Router();

router.post('/', createTuto);
router.get('/', getAllTutos);
router.get('/published', findPublished);
router.get('/:id', getOneTuto);
router.put('/:id', updateTuto);
router.delete('/:id', deleteTuto);


module.exports = router;