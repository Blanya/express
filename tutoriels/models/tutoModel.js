module.exports = (sequelize, Datatypes) => {
    return sequelize.define(
        'Tutoriel', {
            id: {
                type: Datatypes.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            title: {
                type: Datatypes.STRING,
                allowNull : false
            },
            description: {
                type: Datatypes.TEXT,
                allowNull: false
            },
            published: {
                type: Datatypes.BOOLEAN,
                allowNull : false 
            }
        },
        {   timestamps: true,
            createdAd: false,
            updatedAt: false}
    )
}