const {tuto} = require('../config/sequelize');

//GETALL
exports.getAllTutos = (req, res) => {
    tuto.findAll().then(tutoriels =>
        {
            message = "La liste des tutoriels a été récupéré";
            res.json({message: message, data: tutoriels})
        })
};

//GETONE
exports.getOneTuto = (req, res) => {
    const id = req.params.id;
    tuto.findByPk(id).then(tutoriel =>
        {
            message = `Le tutoriel n'${id} a été récupéré`;
            res.json({message: message, data: tutoriel})
        })
};


//POST
exports.createTuto = (req, res) => {
    tuto.create(req.body).then(tutoriel => {
        const message = "Le tutoriel a bien été créé";
        res.json({message: message, data: tutoriel});
    })
}

//PUT
exports.updateTuto = (req, res) => {
    const id = req.params.id;
    const {titre, description, published} = req.body;
    let data = tuto.findByPk(id).then(tutoriel =>
        tutoriel.update({
            title : titre? titre : data.title,
            description : description? description : data.description,
            published: published? published : data.published
        })).then(tutoriel => {
            message = `Le tutoriel n'${id} a été modifié`;
            res.json({message: message, data: tutoriel});
        })
};

//DELETE
exports.deleteTuto = (req, res) => {
    const id = req.params.id;
    tuto.destroy({where: {id: id}}).then(_=> {
        message = `Le tutoriel avec l'id ${id} a bien été supprimé`;
        res.json({message: message});
    })
};

//FindIfPublished
exports.findPublished = (req, res) => {
    tuto.findAll({
        where: {
            published: true
        }
    }).then(tutoriels => {
        message= "Tous les livres publiés ont été récupéré";
        res.json({message: message, data: tutoriels});
    })
}