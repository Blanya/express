module.exports = (sequelize, DataTypes) => {
    return sequelize.define(
        'Pokemon', {
            id: {   type: DataTypes.INTEGER,
                    //non null, auto increment
                    primaryKey: true,
                    autoIncrement: true},
            name: { type: DataTypes.STRING,
                    allowNull : false,},
                    //unique: {msg: "Le pokemon doit avoir un nom unique"}},
            hp: {   type: DataTypes.INTEGER,
                    allowNull: false,
                    validate: {
                        isInt: {msg: "Utilisez des nombres entiers pour les points de vie"},
                        notNull : {msg: "Les points de vie de votre pokemon doivent être supérieur a 0"}
                    }},
            cp: {   type: DataTypes.INTEGER,
                    allowNull: false},
            picture: {  type: DataTypes.STRING,
                        allowNull: false},
            types: {    type: DataTypes.STRING,
                        allowNull: false},
        },
            {   timestamps: true,
                createdAd: 'created',
                updatedAt: false}
    );
}