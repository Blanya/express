const { success } = require('../helpers');
const { Pokemon } = require('../config/sequelize');
const { ValidationError } = require("sequelize"); 

exports.createPokemon = (req, res) => {
    Pokemon.create(req.body).then(pokemon => {
        const message = `Le pokémon ${pokemon.name} a bien été créé`;
        res.json(success(message, pokemon));
    }).catch(error => {
        if(error instanceof ValidationError)
        {
            return res.statut(400).json(success(error.message, error))
        }
        const message = "Le pokémon n'a pu être créé, réessayez dans quelques instants..";
        return res.statut(500).json(success(message, error));
    })
}

exports.getAllPokemons = (req, res) => {
    Pokemon.findAll().then(pokemons => {
        const message = `La liste des pokémons a bien été initialisée`;
        res.json(success(message, pokemons));
    })
}

exports.getPokemon = (req, res) => {
    const id = req.params.id;
    Pokemon.findByPk(id).then(pokemon => {
        const message = `Le pokemon avec l'id ${id} a bien été récupéré`;
        res.json(success(message, pokemon));
    })
}

exports.deletePokemon = (req, res) => {
    const id = req.params.id;
    Pokemon.findByPk(id).then(pokemon => {
        if(pokemon === null){
            const message = "Le pokemon n'existe pas..";
            return res.status(404).json(success(message));
        }
    });
    Pokemon.destroy({where: {id : id}}).then(pokemon => {
        const message = `Le pokemon avec l'id ${id} a bien été supprimé`;
        res.json(success(message));
    })
}

exports.updatePokemon = (req, res) => {
    const id = req.params.id;
    const {name, hp, cp, picture, types} = req.body;
    let data = Pokemon.findByPk(id).then(pokemon => pokemon.update({
        name: name? name : data.name,
        hp: hp? hp : data.hp,
        cp: cp? cp : data.cp,
        picture: picture? picture : data.picture,
        types: types? types.join() : data.types}))
    .then(pokemon => {
        const message = `Le pokemon avec l'id ${pokemon.id} a bien été modifié`;
        res.json(success(message, pokemon));
    });
}


//db json
// const { success, incrementId } = require('../helpers');
// const fs = require('fs');
// const getPokemons = () => {
//     const contentPokemons = fs.readFileSync('pokemon.json');
//     const pokemons = JSON.parse(contentPokemons);
//     return pokemons;
// }

// const savePokemons = (data) => {
//     fs.writeFileSync("pokemon.json", JSON.stringify(data), 'utf-8');
// }

// //getAll
// const getAllPokemons = (req, res) =>{
//     const message = "La liste des pokemons a bien été récupérée";
//     res.json(success(message, getPokemons()));
// };

// //getOne
// const getPokemon = (req, res) => {
//     const message = "Le pokemon a bien été récupéré";
//     const id = Number(req.params.id);
//     const pokemon = getPokemons().find(p => p.id === id);
//     res.json(success(message, pokemon));
// };

// //put
// const updatePokemon = (req, res) =>{
//     const message = "La liste à été modifié";
//     const pokemon = req.body;
//     const id = Number(req.params.id);
//     const data = getPokemons();
//     const poke = data.find(p => p.id === id);
//     data.splice(data.indexOf(poke), 1, pokemon);
//     savePokemons(data);
//     res.json(success(message, pokemon));

//     //const pokemonUpdated = {...req.body, id:id};
//     //const pokemons = getPokemons().map((pokemon)=> {
//     //  return pokemon.id === id? pokemonUpdated : pokemon;    
//     //}
//     //savePokemons(pokemons);
// }; 

// //post
// const addPokemon = (req, res) =>{
//     const message = "Le pokemon a bien été ajouté";

//     const pokemonCreated = {...req.body, ...{id: incrementId(getPokemons()), created: new Date()}}
//     const pokemons = [...getPokemons(), pokemonCreated]

//     // const pokemon = req.body;
//     // const data = getPokemons();
//     // data.push(pokemon);

//     savePokemons(pokemons);
//     res.json(success(message, getPokemons()));
// };

// //delete
// const deletePokemon = (req, res) =>{
//     const message = "Le pokemon a été suppprimé";
//     const id = Number(req.body.id);

//     // data = getPokemons().filter(p => p.id != id);
//     // savePokemons(data)
//     const data = getPokemons();
//     const poke = data.find(p => p.id === id);
//     data.splice(data.indexOf(poke), 1);
//     savePokemons(data);
//     res.json(success(message, data));
// }

// module.exports = {getAllPokemons, getPokemon, deletePokemon, updatePokemon, addPokemon}
