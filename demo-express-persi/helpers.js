exports.success = (message, data) => {
    return {
        message: message,
        data: data,
    }
} 

//IncrementId
exports.incrementId = (poke) => {
    let max = 0;
    poke.forEach(p => p.id>max ? max = p.id : max = max);
    return max+1;
}