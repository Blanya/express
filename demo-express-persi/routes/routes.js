const express = require('express');
const { getAllPokemons, getPokemon, deletePokemon, updatePokemon, createPokemon } = require('../controller/controllerPokemons');

const router = express.Router();

router.get('/', getAllPokemons);
router.get('/:id', getPokemon);
router.post('/', createPokemon);
router.delete('/:id', deletePokemon);
router.put('/:id', updatePokemon);

module.exports = router;