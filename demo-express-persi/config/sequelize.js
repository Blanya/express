const { Sequelize, DataTypes } = require("sequelize");
const pokemonModel = require("../models/pokemonModel");
const pokemons = require('../pokemon.json');
const bcrypt = require('bcrypt');
const userModel = require("../models/userModel");

const sequelize = new Sequelize('poke', 'root', '', {
    host: 'localhost',
    dialect: 'mariadb',
    logging: false
});

const Pokemon = pokemonModel(sequelize, DataTypes);
const User = userModel(sequelize, DataTypes);

//Pour remplir la BDD
const initDb = () => {
    return sequelize.sync({force: true}).then(_=> {
        //_ => ne passe rien, sert à effectuer une action
        pokemons.map( pokemon => {
            Pokemon.create({
                name: pokemon.name,
                hp: pokemon.hp,
                cp: pokemon.cp,
                picture: pokemon.picture,
                types: pokemon.types.join() 
            }).then(pokemon => console.log(pokemon.toJSON()))
        });
        console.log("La BDD a bien été initialisée");
        bcrypt.hash('password', 10 ).then(hash => {
            User.create({
                username: "admin",
                password: hash
            }).then(console.log("Admin créé"));
        });
    })
}

module.exports = {initDb, Pokemon};