const express =  require('express');
const app = express();
const port = 3000;
const bodyparser = require('body-parser');
//midleware pour dev/ connaitre route, status, temps de réponse
const morgan = require('morgan');
const pokeRoutes = require('./routes/routes');
const sequelize = require("./config/sequelize");



// //Middleware 
// const test = (req, res, next) => {
//     console.log("opération effectuée à " + new Date().toLocaleTimeString());
//     next();
// }

// app.use('/api/pokemons/:id', function(req, res, next){
//     console.log('La requête a été effectuée à : ' + new Date().toLocaleTimeString());
//     next();
// })

app
    .use(bodyparser.json())
    .use('/pokemons', pokeRoutes)
    //.use(test)
    .use(morgan("dev"));
    
   

app.get('/', (req, res) => console.log(res.send("Hello, express")));

//sequelize.initDb();
app.listen(port, ()=> console.log(`Votre app démarre sur : http://localhost:${port}`));