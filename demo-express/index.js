// var fs = require('fs');
// var data;

// fs.readFile('input.txt', function(err, data)
// {
//     if(err) return console.error(err);
//     data = data;
//     console.log("Le contenu du fichier est : " + data)
// })

// var out_data = "OutPut line 1.\r\n Output line 2.\r\n OutPut line 3.";

// fs.writeFile('output.txt', out_data, function(err){
//     if(err) return console.error(err);
//     console.log("Le contenu enregistré du fichier est : " + out_data);
// })

const express = require('express');
//instance pour le corps de l'app, pour l'utiliser
const app = express();
//si const, utiliser un fichier json externe
const parkings = require("./parking.json");

//pour manipuler en json
app.use(express.json());

app.get("/parkings", (req, res)=> {
    res.status(200).json(parkings);
})

app.get("/parkings/:id", (req, res)=> {
    const id = Number(req.params.id);
    const parking = parkings.find(parking => parking.id === id);
    res.status(200).json(parking);
})

//Sans persistance de données
app.post("/parkings", (req, res) =>{
    parkings.push(req.body);
    res.status(200).json(parkings);
})

// MA VERSION => MODIF DS L'AFFICHAGE JSON
// app.put("/parkings/:id", (req, res) =>{
//     const id = Number(req.body.id);
//     const parking = req.body;
//     parkings.splice(id-1, 1, parking);
//     res.status(200).json(parkings);
// })

//pas modif ds affichage 
app.put("/parkings/:id", (req, res) =>{
    const id = Number(req.params.id);
    let parking = parkings.find((parking) => parking.id === id);
    parking = req.body;
    res.status(200).json(parking);
})

app.delete("/parkings/:id", (req, res) =>{
    const id = Number(req.params.id);
    const parking = parkings.find((parking)=> parking.id === id);
    parkings.splice(parkings.indexOf(parking), 1);
    res.status(200).json(parkings);
})

app.listen(8080, () => {
    console.log("Serveur en marche")
});